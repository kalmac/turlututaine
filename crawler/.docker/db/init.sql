CREATE extension IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS "article";
DROP SEQUENCE IF EXISTS article_id_seq;
CREATE SEQUENCE article_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."article" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "crawl" uuid NOT NULL,
    "url" text NOT NULL,
    "title" text NOT NULL,
    CONSTRAINT "article_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "crawl";
CREATE TABLE "public"."crawl" (
    "id" uuid DEFAULT uuid_generate_v4(),
    "created_at" timestamp DEFAULT now() NOT NULL,
    "size" integer NOT NULL,
    "state" boolean DEFAULT true NOT NULL,
    "newspaper" character varying(64) NOT NULL,
    CONSTRAINT "crawl_id" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "newspaper";
CREATE TABLE "public"."newspaper" (
    "id" character varying(64) NOT NULL,
    "url" text NOT NULL,
    "language" character varying(6) NOT NULL,
    "created_at" timestamp DEFAULT now() NOT NULL,
    "enabled" boolean DEFAULT true NOT NULL,
    CONSTRAINT "newspaper_id" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "newspaper" ("id", "url", "language", "created_at", "enabled") VALUES
('lemonde',	'https://www.lemonde.fr',	'fr',	'2022-04-03 12:52:55.387895',	'1'),
('liberation',	'https://www.liberation.fr',	'fr',	'2022-04-03 12:52:55.387895',	'1'),
('lefigaro',	'https://www.lefigaro.fr',	'fr',	'2022-04-03 12:52:55.387895',	'1');

ALTER TABLE ONLY "public"."article" ADD CONSTRAINT "article_crawl_fkey" FOREIGN KEY (crawl) REFERENCES crawl(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."crawl" ADD CONSTRAINT "crawl_newspaper_fkey" FOREIGN KEY (newspaper) REFERENCES newspaper(id) NOT DEFERRABLE;

-- 2022-04-03 13:05:47.72445+00
