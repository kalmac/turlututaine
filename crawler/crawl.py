import newspaper

class Crawl:

    QUERY_SELECT_NEWSPAPER = """
        SELECT * from newspaper WHERE enabled = true
        """

    QUERY_INSERT_CRAWL = """
        INSERT INTO crawl ("size", "newspaper") VALUES (%s, %s) RETURNING id
        """

    QUERY_INSERT_ARTICLE = """
        INSERT INTO article ("crawl", "url", "title") VALUES (%s, %s, %s)
        """

    # class default constructor
    def __init__(self, connection):
            self.connection = connection
            self.cursor = connection.cursor()

    #
    # Will start crawl by
    # query the newspaper table
    # and for each newspaper call the
    # crawlNewspaper function
    #
    def run(self):
        self.cursor.execute(self.QUERY_SELECT_NEWSPAPER)
        papers = self.cursor.fetchall()

        for paper in papers:
            self.crawlNewspaper(paper)

    #
    # Crawl a single newspaper
    #
    def crawlNewspaper(self, paper):
        id = paper[0]
        url = paper[1]
        lang = paper[2]

        print("Build newspaper %s (%s)" % (id, url))
        news = newspaper.build(url, language=lang, memoize_articles=False)

        self.cursor.execute(self.QUERY_INSERT_CRAWL, (news.size(), id))
        self.connection.commit()
        crawlId = self.cursor.fetchone()[0]
        print("%s has %d articles in crawl %s" % (id, news.size(), crawlId))

        for article in news.articles:
            try:
                print("Parse %s" % (article.url))
                self.crawlArticle(article, crawlId)

            except (Exception) as error:
                print("Error on url %s" % (article.url), error)

    #
    # Craw a single article
    #
    def crawlArticle(self, article, crawlId):
        article.download()
        article.parse()
        self.cursor.execute(self.QUERY_INSERT_ARTICLE, (crawlId, article.url, article.title))
        self.connection.commit()
        
