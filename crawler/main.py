from dotenv import load_dotenv
import psycopg2
import os
from crawl import Crawl

load_dotenv()
DB_HOST = os.getenv("DB_HOST")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_NAME = os.getenv("DB_NAME")

try:
    conn = psycopg2.connect(
        f"dbname={DB_NAME} user={DB_USER} password={DB_PASSWORD} host={DB_HOST}")

    # Call main function
    crawler = Crawl(conn)
    crawler.run()

except (Exception, psycopg2.Error) as error:
    print("Database error", error)

finally:
    if conn:
        conn.close()

# import newspaper

# # paper = newspaper.build('https://www.lemonde.fr')

# # for article in paper.articles:
# #     article.parse()
# #     print(article.url)

# # for category in paper.category_urls():
# #     print(category)

# article = newspaper.build_article('https://www.lemonde.fr/international/article/2022/04/03/guerre-en-ukraine-autour-de-kiev-scenes-de-desolation-apres-le-retrait-des-forces-russes_6120369_3210.html')
# article.download()
# article.parse()
# print(article.url)
