# Turlututaine crawler

Uses the *perfect* [newspaper3K library](https://github.com/codelucas/newspaper).

## Requirements 

On debian install package `libpq-dev`.

## Installation

```bash
# You can use a virtualenv
# source venv/bin/activate
pip3 install -r requirements.txt
```