# Turlututaine 


```bash
# Launch services (postgres ...)
docker-compose up -d
```

## Databases

### newspaper

* id : (string) PK
* url : string
* language : string
* created_at : date
* enabled : boolean

### newspaper_crawl

* id : PK (uuid)
* newspaper : FK
* date : date
* size : number
* state : enum (SUCCESS / ERROR)

### article

* id : (string) PK
* newspaper : FK
* crawl : FK
* url : string
* title : string
* summary : string
* keywords : jsonb
* top_image : url
* authors : jsonb
* published_at : date
